define [
  "app"
  "controllers/example-controller"
], (app) ->
  ExampleController = require("controllers/example-controller")

  Router = Backbone.Marionette.AppRouter.extend(
    routes:
      "example":  "example"
      "*actions": "index"

    index: ->
      console.log "iam index"
      exampleController = new ExampleController()
      return

    example: ->
      console.log('example')
      exampleController = new ExampleController()
      return
  )
  Router

