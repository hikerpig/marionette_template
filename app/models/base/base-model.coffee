define ["config"], (config) ->
  BaseModel = Backbone.Model.extend({
    urlPath: ''
    apiRoot: config.apiRoot
    url: () ->
      return "#{@apiRoot}#{@urlPath}"
  })
  return BaseModel