define [
  'models/base/base-model'
], (BaseModel) ->

  isErrorField = (key) ->
    return key.match(/^error_/)

  DefaultMessages = {
    invalid: "{0} is invalid!"
    required: "Please input {0}"
    missing_field: "Please input {0}"
  }

  FormModel = BaseModel.extend
    initialize: (opt={}) ->
      BaseModel::initialize.apply(@, arguments)
      if opt.fields
        @fields(opt.fields)

    # 保存所有字段信息，是个有序数组
    fields: (fields) ->
      if _.isUndefined(fields)
        return @get('fields')
      else
        for field in fields
          @labelMap[field.id] = field.label if field.label
        @set('fields', fields)

    field: (id) ->
      return _.findWhere @get('fields'), {id: id}

    addField: (name, modelOrSelector) ->
      @fields.push
        name: name
        $error: {}
        $valid: yes
        $invalid: no
        $pristine: yes
        $dirty: no

    # 主动标记错误
    error: (field, code, message) ->
  #    field = @field(field)
  #    if not field
  #      throw Error "Invalid field"
  #
  #    field.$error[code] = message
  #
      true

      #errorObj = formException.wrapError(field, code, @)
      #if message then errorObj.message = message
      #@errosCount++
      #@trigger('error', errorObj)

    # 解析来自远端的错误
    parseError: (xhr) ->
      # TODO: 解析xhr，并且统一处理

    clearErrors: ->
      _.each @attributes, (value, key) ->
        if isErrorField(key)
          @unset(key)

    beforeValidate: () ->
      @errosCount = 0

    # 错误数量
    errors: ->
      # 看看有没有error_开头的字段，并且有值
      return _.map @attributes, (value, key) ->
        if isErrorField(key)
          return

    # 是否有错误
    hasError: ->
      return @errosCount > 0

  return FormModel
