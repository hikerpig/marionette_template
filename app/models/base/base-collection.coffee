define ["config"], (config) ->
  BaseCollection = Backbone.Collection.extend
    urlPath: ''
    apiRoot: config.apiRoot
    urlRoot: ->
      return "#{@apiRoot}#{@urlPath}"
    url: ->
      return @urlRoot()
  return BaseCollection
