define [
  "controllers/base/base-controller",
  "views/example"
], (BaseController) ->
  # 用作实例的一个controller

  ExampleView = require('views/example')

  ExampleController = BaseController.extend
    initialize: ->
      console.log('ExampleController')
      exampleView = new ExampleView()
      exampleView.render()
      @

  return ExampleController

