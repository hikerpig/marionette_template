define [
  "handlebars"
  "templates"
  "utils/behaviors"
], (Handlebars, Templates, BEHAVIORS) ->
  app = {}
  Layout = {}
  JST = Templates = Templates or {}

  # 定义behaviors的存储
  Backbone.Marionette.Behaviors.behaviorsLookup = ->
    BEHAVIORS.STORE

  ###
  扩展一下Marionette的Renderer, 能够获取到编译以后的template
  @param template
  @param data
  @returns {*}
  ###
  Backbone.Marionette.Renderer.render = (template, data) ->
    maybeObject = JST[template]
    throw "Template '" + template + "' not found!"  unless maybeObject
    tpl = Handlebars.template(maybeObject)
    html = tpl(data)
    html

  app = new Backbone.Marionette.Application()

  MainLayout = Backbone.Marionette.LayoutView.extend(
    el: "#main"
    template: "main-layout"
    regions:
      content: ".contents-container"
  )
  layout = new MainLayout()
  layout.render()
  app

