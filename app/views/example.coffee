define([
  'views/base/base-view'
], (BaseView) ->
  ExampleView = BaseView.extend
    el: '.contents-container'
    template: 'example'
  return ExampleView
)