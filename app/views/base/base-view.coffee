define [], () ->
  BaseView = Backbone.Marionette.View.extend(
    getTemplateData: ->
      templateData = {}
      templateData

    render: ->
      @triggerMethod "before:render"
      Marionette.View::render.apply this, arguments
      templateData = @getTemplateData() or {}
      html = Backbone.Marionette.Renderer.render(@getTemplate(), templateData)
      @$el.html html
      @triggerMethod "render", this
      @

    afterRender: ->
      $('select').selectpicker()
      return @

    onDomRefresh: ->
      console.log @
      @
  )
  BaseView

