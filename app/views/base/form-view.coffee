define [
  'views/base/base-view'
], (BaseView) ->

  FormView = BaseView.extend(
    name: 'FormView'
    ui:
      'form': 'form'

    behaviors:
      Submit:
        'message': 'submiting?'

        validateForSubmit: ($form) ->
          true

        onSubmitSuccess: () ->
          console.log arguments

        onSubmitError: () ->
          console.log arguments

    getFormVariables: ($form) ->
      hash = {}
      if not $form && $form.length
        console.log "no $form in getFormVariables"
        return hash
      serializedObj = $form.serializeArray()
      hash

    save: () ->
      if not @model then return @
      @model.save()
      return @model
  )
  FormView

