define([
  'marionette'
], ()->
  STORE = {}

  # 表单提交行为(behavior)的一个抽象接口
  SubmitBehavior = Marionette.Behavior.extend
    defaults:
      message: 'Submit Behavior'

      submitStart: ->
        true

      submitEnd: ->
        true

      validateForSubmit: ($form) ->
        true

      onSubmitSuccess: () ->
        console.log arguments

      onSubmitError: () ->
        console.log arguments

    ui:
      'form': 'form'

    events:
      'submit @ui.form': 'onSubmit'
      'click @ui.submitBtn': 'onSubmit'

    onSubmit: (e) ->
      # 注意在这里的 this 指向的是 SubmitBehavior 的实例
      # 能够通过this.view获取view的实例 也可以通过this.options获取view.behavios.Submit里的参数
      # 可以在这里打debugger看下
      options = @options
      $cur = $(e.target)
      if @options.validateForSubmit($cur) is false then return
      @options.submitStart()
      if @view.save
        maybeDeferred = @view.save()
        if maybeDeferred && maybeDeferred.always
          maybeDeferred
            .always(options.submitEnd)
            .success(options.onSubmitSuccess)
            .error(options.onSubmitError)

      true


  STORE.Submit = SubmitBehavior

  exports =
    STORE: STORE
  return exports
)
