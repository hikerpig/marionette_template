# Bestminr前端框架

### 版本 V0.0.1

#### 安装

```
npm install -g grunt-cli bower handlebars
gem install sass
bower install
npm install --sav-dev
```

#### Develop

```
grunt develop
```

### 文件夹结构

```
.
├── Gruntfile.coffee  //grunt自动化任务流的配置文件 懒人初级版
├── Makefile  //用makefile写的task 懒人进阶版...
├── app
│   ├── app.coffee   //Backbone.Marionette.application的实例初始化, 以及对Marionette的一些设置
│   ├── config.coffee //全局的配置, 例如apiRoot
│   ├── main.coffee //requirejs 的入口, 配置requirejs的path和开始app
│   ├── controllers
│   │   ├── base
│   │   │   ├── base-controller.coffee
│   │   ├── //.... 其他controller就自己写咯
│   ├── models
│   │   ├── base
│   │   │   ├── base-collection.coffee
│   │   │   ├── base-model.coffee
│   │   │   └── form-model.coffee  //给FormView用的model, save就相当于保存表单
│   │   ├── //.... 其他models就自己写咯
│   ├── router.coffee
│   ├── styles
│   │   └── main.scss
│   ├── img // 图片文件夹
│   ├── templates // handlebars模板文件夹
│   │   ├── company-item.hbs
│   ├── utils
│   │   └── behaviors.coffee  // 集中存放Marionette的behavior扩展们, 相当于给View使用的事件抽象接口
│   └── views
│       └── base
│           ├── base-collection-view.coffee
│           ├── base-item-view.coffee
│           ├── base-view.coffee
│           └── form-view.coffee
├── bower.json  // 指定前端依赖库的配置文件
├── index.html
├── package.json
└── vendor  // 手动添加的第三方库, 可能由于bower未收录或者自己对库做了一些改动, 加入以后记住在main.coffee里指定path哟
```

### 不需要加到git仓库中的文件

- `appjs/`    coffee编译成的js
- `build/`    html, handlebars模板, css文件编译的去处