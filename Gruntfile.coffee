proxySnippet = require("grunt-connect-proxy/lib/utils").proxyRequest

# 一些配置
CONNECT_OPTIONS =
  port: 9002 # 本地预览时候的端口号
  hostname: "0.0.0.0"  #本地的运行时候的主机名, 设为0.0.0.0的时候, 同一局域网的同事可以用你的ip:端口来随时预览你的成果

CONNECT_PROXIES = [{
  context: "/neo4j/api/v1" # api地址要自己配置
  host: "http://119.2.0.196" #api 的主机名
  port: 9004 #api 的端口号
}]

module.exports = (grunt) ->
  require('time-grunt')(grunt)


  grunt.initConfig
    yeoman:
      app: "app"
      dist: "build"
      appjs: "appjs"

    watch:
      options:
        nospawn: true
        livereload: true

      handlebars:
        files: ["app/templates/**/*.hbs"]
        tasks: ["handlebars"]

      scss:
        files: "app/styles/**/*.scss"
        tasks: ["sass"]

      coffee:
        files: "app/**/*.coffee"
        tasks: ["newer:coffee"]

      html:
        files: "*.html"
        tasks: ["processhtml:dev"]

      livereload:
        files: [
          #'*.html',
          "app/styles/{,*/}*.css"
          "app/{,*/}*.js"
        ]

    clean:
      coffee: ["appjs"]
      dist: ["build"]

    jshint:
      all: [
        "Gruntfile.js"
        "app/**/*.js"
        "app/modules/**/*.js"
      ]

    requirejs:
      compile:
        options:
          baseUrl: "<%= yeoman.appjs %>"
          mainConfigFile: "<%= yeoman.appjs %>/main.js"
          include: "main"
          name: "../bower_components/almond/almond"
          out: "build/scripts/prod.js"

    sass:
      dist:
        files:
          "build/styles/main.css": "app/styles/main.scss"

    handlebars:
      files:
        src: "app/templates/**/*.hbs"
        dest: "build/templates.js"
        options:
          amd: true
          wrapped: false
          #namespace: 'JST',
          processName: (filePath) ->
            outName = filePath
            outName = outName.split(".").slice(0, -1).join(".")  if outName.indexOf(".hbs") > -1
            outName = outName.replace("app/templates/", "")  if outName.indexOf("app/templates/") > -1
            console.log 'compiled template: ' + outName
            outName

    coffee:
      options:
        bare: true

      build:
        expand: true
        cwd: 'app'
        src: ['**/*.coffee']
        dest: "<%= yeoman.appjs %>"
        ext: '.js'

    copy:
      fonts:
        files:[{
          expand: true
          flatten: true
          src: [
            './bower_components/fontawesome/fonts/*'
          ]
          dest: "<%= yeoman.dist %>/fonts/"
        }]

      # 对应的是bower库里的各个images文件夹
      #images:
        #files:[{
          #expand: true
          #flatten: true
          #src: [
          #]
          #dest: "<%= yeoman.dist %>/images/"
        #}]

      img:
        files:[{
          expand: true
          flatten: true
          src: ['<%= yeoman.app %>/img/**/*' ]
          dest: "<%= yeoman.dist %>/img/"
        }]

    cssmin:
      dist:
        files:
          "build/styles/main.css": [
            "build/styles/main.css"
          ],
          "build/styles/vendor.css": [
            "bower_components/fontawesome/css/font-awesome.css"
          ]

    processhtml:
      dev:
        options:
          process: true
          data:
            title: "Demo developing"
            script: "<script data-main=\"<%= yeoman.appjs %>/main\" src=\"bower_components/requirejs/require.js\"></script>"

        files:
          "build/index.html": ["index.html"]

      dist:
        options:
          process: true
          data:
            title: "Demo"
            script: "<script src=\"scripts/prod.js\"></script>"

        files:
          "build/index.html": ["index.html"]

    connect:
      options:
        port: CONNECT_OPTIONS.port
        hostname: CONNECT_OPTIONS.hostname
        livereload: 35729

      proxies: CONNECT_PROXIES

      dist:
        options:
          middleware: (connect, options) ->
            middlewares = []
            middlewares.push proxySnippet
            options.base.forEach (base) ->
              middlewares.push connect.static(base)
              return

            #middlewares.push (req, res, next) ->
              #console.log(req.url)

              #next()

            middlewares

          base: [
            "build"
            "."
          ]

  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-connect-proxy"
  grunt.loadNpmTasks "grunt-contrib-cssmin"
  grunt.loadNpmTasks "grunt-processhtml"
  grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-copy"
  grunt.loadNpmTasks "grunt-requirejs"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-handlebars-2"
  grunt.loadNpmTasks "grunt-contrib-sass"
  grunt.loadNpmTasks "grunt-newer"

  grunt.registerTask "build", [
    "clean:dist"
    "handlebars"
    "sass"
    "coffee:build"
    "requirejs"
    "cssmin"
    "copy"
    "processhtml:dist"
  ]
  grunt.registerTask "dev-build", [
    "handlebars"
    "sass"
    "coffee:build"
    "cssmin"
    "copy"
    "processhtml:dev"
  ]
  grunt.registerTask "develop", [
    "dev-build"
    "configureProxies"
    "connect:dist:keep-alive"
    "watch"
  ]
  grunt.registerTask "release", [
    "build"
  ]
  return
